var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Pattern = /** @class */ (function () {
    function Pattern() {
    }
    // constructor(pattern: string, size: number) {
    //    // console.clear()
    //    // console.log('Pattern yang dipilih = ' + pattern);
    //    // alert('Pattern : ' + pattern + 'ukuran : ' + size);
    //    switch (pattern) {
    //       case '1':
    //          this.pattern1(size)
    //          break;
    //       case '2':
    //          this.pattern2(size)
    //          break;
    //       case '3':
    //          this.pattern3(size)
    //          break;
    //       case '4':
    //          this.pattern4(size)
    //          break;
    //       case '5':
    //          this.pattern5(size)
    //          break;
    //       case '6':
    //          this.pattern6(size)
    //          break;
    //       case '7':
    //          this.pattern7(size)
    //          break;
    //       case '8':
    //          this.pattern8(size)
    //          break;
    //       case '9':
    //          this.pattern9(size)
    //          break;
    //       case '10':
    //          this.pattern10(size)
    //          break;
    //       case '11':
    //          this.pattern11(size)
    //          break;
    //       case '12':
    //          this.pattern12(size)
    //          break;
    //       case '13':
    //          this.pattern13(size)
    //          break;
    //       case '14':
    //          this.pattern14(size)
    //          break;
    //       case '15':
    //          this.pattern15(size)
    //          break;
    //       case '16':
    //          this.pattern16(size)
    //          break;
    //       case '17':
    //          this.pattern17(size)
    //          break;
    //       case '18':
    //          this.pattern18(size)
    //          break;
    //       case '19':
    //          this.pattern19(size)
    //          break;
    //       case '20':
    //          this.pattern20(size)
    //          break;
    //       case '21':
    //          this.pattern21(size)
    //          break;
    //       case '22':
    //          this.pattern22(size)
    //          break;
    //       case '23':
    //          this.pattern23(size)
    //          break;
    //       case '24':
    //          this.pattern24(size)
    //          break;
    //       case '25':
    //          this.pattern25(size)
    //          break;
    //       case '26':
    //          this.pattern26(size)
    //          break;
    //       case '27':
    //          this.pattern27(size)
    //          break;
    //       case '28':
    //          this.pattern28(size)
    //          break;
    //       case '29':
    //          this.pattern29(size)
    //          break;
    //       case '30':
    //          this.pattern30(size)
    //          break;
    //       case '31':
    //          this.pattern31(size)
    //          break;
    //       default:
    //          alert('Pattern tidak ada');
    //          break;
    //    }
    // }
    Pattern.prototype.pattern1 = function (size) {
        var size = size;
        var jmlStar = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar++;
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern2 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            for (var j = 0; j < fronSpace; j++) {
                output = output + '&nbsp;&nbsp;';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar++;
            fronSpace--;
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern3 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar--;
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern4 = function (size) {
        var size = size;
        var jmlStar = size;
        var fronSpace = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            for (var j = 0; j < fronSpace; j++) {
                output = output + '&nbsp;&nbsp;';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            output = output + '<br>';
            jmlStar--;
            fronSpace++;
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern5 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = size;
        var output = '';
        for (var i = 0; i < size; i++) {
            for (var j = 0; j < fronSpace; j++) {
                output = output + '&nbsp;&nbsp;';
            }
            for (var j = 0; j <= jmlStar; j++) {
                output = output + '*';
            }
            jmlStar += 2;
            fronSpace--;
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern6 = function (size) {
        var size = size;
        var jmlStar = (size * 2) - 1;
        var fronSpace = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            for (var j = 0; j < fronSpace; j++) {
                output = output + '&nbsp;&nbsp;';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            jmlStar -= 2;
            fronSpace++;
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern7 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = size / 2;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar += 2;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar -= 2;
                fronSpace++;
            }
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern8 = function (size) {
        var size = size;
        var jmlStar = 0;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
            }
            else {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
            }
            output = output + '<br>';
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern9 = function (size) {
        var size = size;
        var jmlStar = 0;
        var output = '';
        var fronSpace = size / 2;
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
                fronSpace++;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern10 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        var fronSpace = size;
        for (var i = 0; i < size; i++) {
            for (var j = 0; j < fronSpace; j++) {
                output = output + '&nbsp;&nbsp;';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            fronSpace--;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern11 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        var fronSpace = 0;
        for (var i = 0; i < size; i++) {
            for (var j = 0; j < fronSpace; j++) {
                output = output + '&nbsp;&nbsp;';
            }
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            fronSpace++;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern12 = function (size) {
        var size = size;
        var jmlStar = (size / 2);
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern13 = function (size) {
        var size = size;
        var jmlStar = (size / 2);
        var frontSpace = 1;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < frontSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar--;
                frontSpace++;
            }
            else {
                for (var j = 0; j < frontSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
                jmlStar++;
                frontSpace--;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern14 = function (size) {
        var size = size;
        var jmlStar = size / 2;
        var fronSpace = 1;
        var c = jmlStar - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*&nbsp;&nbsp;';
                }
                jmlStar--;
                fronSpace++;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*&nbsp;&nbsp;';
                }
                jmlStar++;
                fronSpace--;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern15 = function (size) {
        var size = size;
        var jmlStar = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            if (i == size) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            jmlStar++;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern16 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            if (i == size) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            jmlStar++;
            fronSpace--;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern17 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i <= size; i++) {
            if (i == 0) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            jmlStar--;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern18 = function (size) {
        var size = size;
        var jmlStar = size;
        var frontSpace = 0;
        var output = '';
        for (var i = 0; i <= size; i++) {
            if (i == 0) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < frontSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            jmlStar--;
            frontSpace++;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern19 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = size - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            if (i == size - 1) {
                for (var j = 0; j <= jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            jmlStar += 2;
            fronSpace--;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern20 = function (size) {
        var size = size;
        var jmlStar = (size * 2) - 1;
        var fronSpace = 0;
        var output = '';
        for (var i = 0; i < size; i++) {
            if (i == 0) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            jmlStar -= 2;
            fronSpace++;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern21 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = (size / 2) - 1;
        var output = '';
        var c = fronSpace;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                jmlStar += 2;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                jmlStar -= 2;
                fronSpace++;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern22 = function (size) {
        var size = size;
        var jmlStar = 0;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                jmlStar++;
            }
            else {
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                jmlStar--;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern23 = function (size) {
        var size = size;
        var jmlStar = 0;
        var output = '';
        var frontSpace = (size / 2) - 1;
        var c = frontSpace;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j <= frontSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                frontSpace--;
                jmlStar++;
            }
            else {
                for (var j = 0; j <= frontSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                jmlStar--;
                frontSpace++;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern24 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        var fronSpace = size;
        for (var i = 0; i < size; i++) {
            if (i == 0 || i == size - 1) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            fronSpace--;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern25 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        var fronSpace = 0;
        for (var i = 0; i < size; i++) {
            if (i == 0 || i == size - 1) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            fronSpace++;
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern26 = function (size) {
        var size = size;
        var jmlStar = size;
        var star = 0;
        var c = (size / 2) - 1;
        var output = '';
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                jmlStar -= 2;
                star++;
            }
            else {
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
                for (var j = 0; j < star; j++) {
                    output = output + '*';
                }
                jmlStar += 2;
                star--;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern27 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = size / 2;
        var output = '';
        var c = (size / 2) - 1;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                jmlStar += 2;
                fronSpace--;
            }
            else if (i == c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '&nbsp;&nbsp;';
                }
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '*';
                }
                jmlStar -= 2;
                fronSpace++;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern28 = function (size) {
        var size = size;
        var jmlStar = 0;
        var fronSpace = (size / 2) - 1;
        var output = '';
        var c = fronSpace;
        for (var i = 0; i < size; i++) {
            if (i < c) {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;';
                    }
                }
                jmlStar += 2;
                fronSpace--;
            }
            else {
                for (var j = 0; j < fronSpace; j++) {
                    output = output + '&nbsp;';
                }
                for (var j = 0; j <= jmlStar; j++) {
                    if (j == 0 || j == jmlStar) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;';
                    }
                }
                jmlStar -= 2;
                fronSpace++;
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern29 = function (size) {
        var star = '';
        var baris = size;
        var jmlStar = baris;
        var frnSpace = 2;
        var showStar = true;
        var c = (baris / 2) - 1;
        for (var i = 0; i < baris; i++) {
            if (i < c) {
                if (i == 0) {
                    for (var j = 0; j < frnSpace; j++) {
                        star = star + '&nbsp;&nbsp;';
                    }
                    showStar = true;
                    for (var j = 0; j < jmlStar; j++) {
                        if (showStar == true) {
                            star = star + '*';
                            showStar = false;
                        }
                        else {
                            star = star + '&nbsp;&nbsp;';
                            showStar = true;
                        }
                    }
                }
                else {
                    for (var j = 0; j < frnSpace; j++) {
                        star = star + '&nbsp;&nbsp;';
                    }
                    for (var j = 0; j < jmlStar; j++) {
                        if (j == 0 || j == jmlStar - 1) {
                            star = star + '*';
                        }
                        else {
                            star = star + '&nbsp;&nbsp;';
                        }
                    }
                }
                frnSpace++;
                jmlStar -= 2;
            }
            else {
                if (i == baris - 1) {
                    for (var j = 0; j < frnSpace; j++) {
                        star = star + '&nbsp;&nbsp;';
                    }
                    showStar = true;
                    for (var j = 0; j < jmlStar; j++) {
                        if (showStar == true) {
                            star = star + '*';
                            showStar = false;
                        }
                        else {
                            star = star + '&nbsp;&nbsp;';
                            showStar = true;
                        }
                    }
                }
                else {
                    for (var j = 0; j < frnSpace; j++) {
                        star = star + '&nbsp;&nbsp;';
                    }
                    for (var j = 0; j < jmlStar; j++) {
                        if (j == 0 || j == jmlStar - 1) {
                            star = star + '*';
                        }
                        else {
                            star = star + '&nbsp;&nbsp;';
                        }
                    }
                }
                frnSpace--;
                jmlStar += 2;
            }
            // console.log(star)
            star = star + "<br>";
        }
        document.getElementById("canvas").innerHTML = star;
    };
    Pattern.prototype.pattern30 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i < size; i++) {
            for (var j = 0; j < jmlStar; j++) {
                output = output + '*';
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    Pattern.prototype.pattern31 = function (size) {
        var size = size;
        var jmlStar = size;
        var output = '';
        for (var i = 0; i < size; i++) {
            if (i == 0 || i == size - 1) {
                for (var j = 0; j < jmlStar; j++) {
                    output = output + '*';
                }
            }
            else {
                for (var j = 0; j < jmlStar; j++) {
                    if (j == 0 || j == jmlStar - 1) {
                        output = output + '*';
                    }
                    else {
                        output = output + '&nbsp;&nbsp;';
                    }
                }
            }
            output = output + "<br>";
        }
        document.getElementById("canvas").innerHTML = output;
    };
    return Pattern;
}());
var Menu = /** @class */ (function (_super) {
    __extends(Menu, _super);
    function Menu() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Menu.prototype.selector = function (pattern, size) {
        switch (pattern) {
            case '1':
                this.pattern1(size);
                break;
            case '2':
                this.pattern2(size);
                break;
            case '3':
                this.pattern3(size);
                break;
            case '4':
                this.pattern4(size);
                break;
            case '5':
                this.pattern5(size);
                break;
            case '6':
                this.pattern6(size);
                break;
            case '7':
                this.pattern7(size);
                break;
            case '8':
                this.pattern8(size);
                break;
            case '9':
                this.pattern9(size);
                break;
            case '10':
                this.pattern10(size);
                break;
            case '11':
                this.pattern11(size);
                break;
            case '12':
                this.pattern12(size);
                break;
            case '13':
                this.pattern13(size);
                break;
            case '14':
                this.pattern14(size);
                break;
            case '15':
                this.pattern15(size);
                break;
            case '16':
                this.pattern16(size);
                break;
            case '17':
                this.pattern17(size);
                break;
            case '18':
                this.pattern18(size);
                break;
            case '19':
                this.pattern19(size);
                break;
            case '20':
                this.pattern20(size);
                break;
            case '21':
                this.pattern21(size);
                break;
            case '22':
                this.pattern22(size);
                break;
            case '23':
                this.pattern23(size);
                break;
            case '24':
                this.pattern24(size);
                break;
            case '25':
                this.pattern25(size);
                break;
            case '26':
                this.pattern26(size);
                break;
            case '27':
                this.pattern27(size);
                break;
            case '28':
                this.pattern28(size);
                break;
            case '29':
                this.pattern29(size);
                break;
            case '30':
                this.pattern30(size);
                break;
            case '31':
                this.pattern31(size);
                break;
            default:
                alert('Pattern tidak ada');
                break;
        }
    };
    return Menu;
}(Pattern));
var button = document.querySelector("button");
button.addEventListener("click", handleClick);
function handleClick() {
    var pattern = document.getElementById("pattern").value;
    var size = parseInt(document.getElementById("size").value);
    if (pattern != '' && size) {
        var p = new Menu;
        p.selector(pattern, size);
    }
    else {
        alert('Pilih pattern dan isi ukuran');
    }
}
