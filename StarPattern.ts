class Pattern {
   // constructor(pattern: string, size: number) {
   //    // console.clear()
   //    // console.log('Pattern yang dipilih = ' + pattern);
   //    // alert('Pattern : ' + pattern + 'ukuran : ' + size);

   //    switch (pattern) {
   //       case '1':
   //          this.pattern1(size)
   //          break;
   //       case '2':
   //          this.pattern2(size)
   //          break;
   //       case '3':
   //          this.pattern3(size)
   //          break;
   //       case '4':
   //          this.pattern4(size)
   //          break;
   //       case '5':
   //          this.pattern5(size)
   //          break;
   //       case '6':
   //          this.pattern6(size)
   //          break;
   //       case '7':
   //          this.pattern7(size)
   //          break;
   //       case '8':
   //          this.pattern8(size)
   //          break;
   //       case '9':
   //          this.pattern9(size)
   //          break;
   //       case '10':
   //          this.pattern10(size)
   //          break;
   //       case '11':
   //          this.pattern11(size)
   //          break;
   //       case '12':
   //          this.pattern12(size)
   //          break;
   //       case '13':
   //          this.pattern13(size)
   //          break;
   //       case '14':
   //          this.pattern14(size)
   //          break;
   //       case '15':
   //          this.pattern15(size)
   //          break;
   //       case '16':
   //          this.pattern16(size)
   //          break;
   //       case '17':
   //          this.pattern17(size)
   //          break;
   //       case '18':
   //          this.pattern18(size)
   //          break;
   //       case '19':
   //          this.pattern19(size)
   //          break;
   //       case '20':
   //          this.pattern20(size)
   //          break;
   //       case '21':
   //          this.pattern21(size)
   //          break;
   //       case '22':
   //          this.pattern22(size)
   //          break;
   //       case '23':
   //          this.pattern23(size)
   //          break;
   //       case '24':
   //          this.pattern24(size)
   //          break;
   //       case '25':
   //          this.pattern25(size)
   //          break;
   //       case '26':
   //          this.pattern26(size)
   //          break;
   //       case '27':
   //          this.pattern27(size)
   //          break;
   //       case '28':
   //          this.pattern28(size)
   //          break;
   //       case '29':
   //          this.pattern29(size)
   //          break;
   //       case '30':
   //          this.pattern30(size)
   //          break;
   //       case '31':
   //          this.pattern31(size)
   //          break;
      
   //       default:
   //          alert('Pattern tidak ada');
   //          break;
   //    }
   // }

   pattern1(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar++;
         output = output + '<br>';
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern2(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         for (let j = 0; j < fronSpace; j++) {
            output = output + '&nbsp;&nbsp;';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar++;
         fronSpace--;
         output = output + '<br>';
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern3(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar--;
         output = output + '<br>';
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern4(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var fronSpace: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         for (let j = 0; j < fronSpace; j++) {
            output = output + '&nbsp;&nbsp;';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         output = output + '<br>'
         jmlStar--
         fronSpace++;
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern5(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = size;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         for (let j = 0; j < fronSpace; j++) {
            output = output + '&nbsp;&nbsp;';
         }
         for (let j = 0; j <= jmlStar; j++) {
            output = output + '*';
         }
         jmlStar += 2;
         fronSpace--;
         output = output + '<br>'
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern6(size: number) {
      var size: number = size;
      var jmlStar: number = (size * 2) - 1;
      var fronSpace: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         for (let j = 0; j < fronSpace; j++) {
            output = output + '&nbsp;&nbsp;';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         jmlStar -= 2;
         fronSpace++;
         output = output + '<br>'
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern7(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = size / 2;
      var output: string = '';
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar += 2;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar -= 2;
            fronSpace++;
         }
         output = output + '<br>'
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern8(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var output: string = '';
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
         } else {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
         }
         output = output + '<br>'
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern9(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var output: string = '';
      var fronSpace: number = size / 2;
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
            fronSpace++;
         }
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern10(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = size;

      for (let i = 0; i < size; i++) {
         for (let j = 0; j < fronSpace; j++) {
            output = output + '&nbsp;&nbsp;';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         fronSpace--;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern11(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = 0;

      for (let i = 0; i < size; i++) {
         for (let j = 0; j < fronSpace; j++) {
            output = output + '&nbsp;&nbsp;';
         }
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         fronSpace++;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern12(size: number) {
      var size: number = size;
      var jmlStar: number = (size / 2);
      var output: string = '';
      var c: number = (size / 2)-1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
         } else {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
         }         
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern13(size: number) {
      var size: number = size;
      var jmlStar: number = (size / 2);
      var frontSpace: number = 1;
      var output: string = '';
      var c: number = (size / 2)-1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < frontSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar--;
            frontSpace++;
         } else {
            for (let j = 0; j < frontSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
            jmlStar++;
            frontSpace--;
         }       
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern14(size: number) {
      var size: number = size;
      var jmlStar: number = size / 2;
      var fronSpace: number = 1;
      var c: number = jmlStar - 1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*&nbsp;&nbsp;';
            }
            jmlStar--;
            fronSpace++;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*&nbsp;&nbsp;';
            }
            jmlStar++;
            fronSpace--;
         }    
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern15(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         if (i == size) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         
         jmlStar++;  
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern16(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         if (i == size) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         
         jmlStar++;
         fronSpace--;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern17(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         if (i == 0) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         
         jmlStar--;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern18(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var frontSpace: number = 0;
      var output: string = '';

      for (let i = 0; i <= size; i++) {
         if (i == 0) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < frontSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         
         jmlStar--;
         frontSpace++;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern19(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = size-1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         if (i == size-1) {
            for (let j = 0; j <= jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }

         
         jmlStar += 2;
         fronSpace--;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern20(size: number) {
      var size: number = size;
      var jmlStar: number = (size * 2) - 1;
      var fronSpace: number = 0;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         if (i == 0) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         
         jmlStar -= 2;
         fronSpace++;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern21(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = (size / 2) - 1;
      var output: string = '';
      var c: number = fronSpace;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            jmlStar += 2;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            jmlStar -= 2;
            fronSpace++;
         }    
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern22(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var output: string = '';
      var c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            jmlStar++;
         } else {
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            jmlStar--;
         }       
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern23(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var output: string = '';
      var frontSpace: number = (size / 2) - 1;
      var c: number = frontSpace;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j <= frontSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            frontSpace--;
            jmlStar++;
         } else {
            for (let j = 0; j <= frontSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            jmlStar--;
            frontSpace++;
         } 
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern24(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = size;

      for (let i = 0; i < size; i++) {
         if (i == 0 || i == size-1) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }         
         fronSpace--;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern25(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';
      var fronSpace: number = 0;

      for (let i = 0; i < size; i++) {
         if (i == 0 || i == size-1) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         fronSpace++;
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern26(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var star: number = 0;
      var c: number = (size / 2) - 1;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
            jmlStar -= 2;
            star++;
         } else {
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
            for (let j = 0; j < star; j++) {
               output = output + '*';
            }
            jmlStar += 2;
            star--;
         }
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern27(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = size / 2;
      var output: string = '';
      let c: number = (size / 2) - 1;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            jmlStar += 2;
            fronSpace--;
         } else if (i == c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            for (let j = 0; j < jmlStar; j++) {
               output = output + '&nbsp;&nbsp;';
            }
            for (let j = 0; j < fronSpace; j++) {
               output = output + '*';
            }
            jmlStar -= 2;
            fronSpace++;
         }
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern28(size: number) {
      var size: number = size;
      var jmlStar: number = 0;
      var fronSpace: number = (size / 2) - 1;
      var output: string = '';
      var c: number = fronSpace;

      for (let i = 0; i < size; i++) {
         if (i < c) {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;';
               }
            }
            jmlStar += 2;
            fronSpace--;
         } else {
            for (let j = 0; j < fronSpace; j++) {
               output = output + '&nbsp;';
            }
            for (let j = 0; j <= jmlStar; j++) {
               if (j == 0 || j == jmlStar) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;';
               }
            }
            jmlStar -= 2;
            fronSpace++;
         } 
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern29(size: number) {
      var star: string = ''
      var baris: number = size
      var jmlStar: number = baris
      var frnSpace: number = 2
      var showStar: boolean = true
      var c: number = (baris/2)-1

      for (var i:number = 0; i < baris; i++) {
         if (i < c) {
            if (i == 0) {
               for (var j:number = 0; j < frnSpace; j++) {
                  star = star + '&nbsp;&nbsp;'
               }
               showStar = true
               for (var j:number = 0; j < jmlStar; j++) {
                  if (showStar == true) {
                     star = star + '*'
                     showStar = false
                  } else {
                     star = star + '&nbsp;&nbsp;'
                     showStar = true
                  }
               }
            } else {
               for (var j:number = 0; j < frnSpace; j++) {
                  star = star + '&nbsp;&nbsp;'
               }
               for (var j:number = 0; j < jmlStar; j++) {
                  
                  if (j == 0 || j == jmlStar-1) {
                     star = star + '*'
                  } else {
                     star = star + '&nbsp;&nbsp;'
                  }
               }
            }
            frnSpace++
            jmlStar-=2
         } else {
            if (i == baris-1) {
               for (var j:number = 0; j < frnSpace; j++) {
                  star = star + '&nbsp;&nbsp;'
               }
               showStar = true
               for (var j:number = 0; j < jmlStar; j++) {
                  if (showStar == true) {
                     star = star + '*'
                     showStar = false
                  } else {
                     star = star + '&nbsp;&nbsp;'
                     showStar = true
                  }
               }
            } else {
               for (var j:number = 0; j < frnSpace; j++) {
                  star = star + '&nbsp;&nbsp;'
               }
               for (var j:number = 0; j < jmlStar; j++) {
                  
                  if (j == 0 || j == jmlStar-1) {
                     star = star + '*'
                  } else {
                     star = star + '&nbsp;&nbsp;'
                  }
               }
            }
            frnSpace--
            jmlStar+=2
         }
         // console.log(star)
         star = star + "<br>";
      }
      document.getElementById("canvas").innerHTML=star;
   }

   pattern30(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         for (let j = 0; j < jmlStar; j++) {
            output = output + '*';
         }
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

   pattern31(size: number) {
      var size: number = size;
      var jmlStar: number = size;
      var output: string = '';

      for (let i = 0; i < size; i++) {
         if (i == 0 || i == size-1) {
            for (let j = 0; j < jmlStar; j++) {
               output = output + '*';
            }
         } else {
            for (let j = 0; j < jmlStar; j++) {
               if (j == 0 || j == jmlStar-1) {
                  output = output + '*';
               } else {
                  output = output + '&nbsp;&nbsp;';
               }
            }
         }
         output = output + "<br>";
      }
      document.getElementById("canvas").innerHTML=output;
   }

}

class Menu extends Pattern {
   selector (pattern: string, size: number) {
      switch (pattern) {
         case '1':
            this.pattern1(size)
            break;
         case '2':
            this.pattern2(size)
            break;
         case '3':
            this.pattern3(size)
            break;
         case '4':
            this.pattern4(size)
            break;
         case '5':
            this.pattern5(size)
            break;
         case '6':
            this.pattern6(size)
            break;
         case '7':
            this.pattern7(size)
            break;
         case '8':
            this.pattern8(size)
            break;
         case '9':
            this.pattern9(size)
            break;
         case '10':
            this.pattern10(size)
            break;
         case '11':
            this.pattern11(size)
            break;
         case '12':
            this.pattern12(size)
            break;
         case '13':
            this.pattern13(size)
            break;
         case '14':
            this.pattern14(size)
            break;
         case '15':
            this.pattern15(size)
            break;
         case '16':
            this.pattern16(size)
            break;
         case '17':
            this.pattern17(size)
            break;
         case '18':
            this.pattern18(size)
            break;
         case '19':
            this.pattern19(size)
            break;
         case '20':
            this.pattern20(size)
            break;
         case '21':
            this.pattern21(size)
            break;
         case '22':
            this.pattern22(size)
            break;
         case '23':
            this.pattern23(size)
            break;
         case '24':
            this.pattern24(size)
            break;
         case '25':
            this.pattern25(size)
            break;
         case '26':
            this.pattern26(size)
            break;
         case '27':
            this.pattern27(size)
            break;
         case '28':
            this.pattern28(size)
            break;
         case '29':
            this.pattern29(size)
            break;
         case '30':
            this.pattern30(size)
            break;
         case '31':
            this.pattern31(size)
            break;
      
         default:
            alert('Pattern tidak ada');
            break;
      }
   }
}


const button = document.querySelector("button");
button.addEventListener("click", handleClick);

function handleClick() {
   var pattern: string = (<HTMLInputElement>document.getElementById("pattern")).value;
   var size: number = parseInt((<HTMLInputElement>document.getElementById("size")).value);

   if (pattern != '' && size) {
      const p = new Menu;
      p.selector(pattern, size);
   } else {
      alert('Pilih pattern dan isi ukuran');
   }
}